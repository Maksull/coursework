DELIMITER $$
CREATE TRIGGER insert_tenant
    AFTER INSERT ON tenants
    FOR EACH ROW 
    BEGIN
        SET @paid = (SELECT rent_per_person FROM apartments_rent_per_person WHERE apartment_id = NEW.apartment_id);
        INSERT INTO tenants_payments(tenant_id, payment) VALUES (NEW.tenant_id, @paid);
    END$$
DELIMITER ;