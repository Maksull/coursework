DELIMITER $$
CREATE TRIGGER delete_tenant
    BEFORE DELETE ON tenants
    FOR EACH ROW 
    BEGIN
        SET @arrival = (SELECT arrival FROM residence WHERE tenant_id = OLD.tenant_id);
        SET @departure = (SELECT departure FROM residence WHERE tenant_id = OLD.tenant_id);
        SET @total_payment = (SELECT SUM(payment) FROM tenants_payments WHERE tenant_id = OLD.tenant_id);
        INSERT INTO residence_history(apartment_id, first_name, last_name, phone_number, total_payment, arrival, departure) 
        VALUES (OLD.apartment_id, OLD.first_name, OLD.last_name, OLD.phone_number, @total_payment, @arrival, @departure);
    END$$
DELIMITER ;