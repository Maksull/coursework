DELIMITER $$
CREATE TRIGGER insert_tenant_payment
    BEFORE INSERT ON tenants_payments
    FOR EACH ROW 
    BEGIN
        SET @rent = TENANT_RENT(NEW.tenant_id);
        SET new.rent = @rent;
    END$$
DELIMITER ;