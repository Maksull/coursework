CREATE TABLE houses (
    house_id INT AUTO_INCREMENT,
    address CHAR(40) NOT NULL UNIQUE,
    PRIMARY KEY (house_id)
);
CREATE TABLE apartments (
    apartment_id INT AUTO_INCREMENT,
    house_id INT NOT NULL, 
    size INT NOT NULL, 
    PRIMARY KEY (apartment_id),
    FOREIGN KEY (house_id) REFERENCES houses(house_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE tenants (
    tenant_id INT AUTO_INCREMENT, 
    apartment_id INT NOT NULL,
    first_name CHAR(30) NOT NULL,
    last_name CHAR(30) NOT NULL, 
    phone_number CHAR(20) NOT NULL, 
    PRIMARY KEY (tenant_id), 
    FOREIGN KEY (apartment_id) REFERENCES apartments(apartment_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE houses_costs (
    id INT AUTO_INCREMENT,
    house_id INT NOT NULL,
    expensive_item VARCHAR(30) DEFAULT "Unidentified",
    cost INT DEFAULT 0,
    UNIQUE (house_id, expensive_item),
    PRIMARY KEY (id),
    FOREIGN KEY (house_id) REFERENCES houses(house_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE apartments_costs (
    id INT AUTO_INCREMENT,
    apartment_id INT NOT NULL,
    expensive_item VARCHAR(30) DEFAULT "Unidentified",
    cost INT DEFAULT 0,
    UNIQUE (apartment_id, expensive_item),
    PRIMARY KEY (id),
    FOREIGN KEY (apartment_id) REFERENCES apartments(apartment_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE tenants_payments (
    id INT AUTO_INCREMENT,
    tenant_id INT NOT NULL,
    date DATE DEFAULT(CURRENT_DATE),
    payment INT NOT NULL,
    rent INT,
    PRIMARY KEY (id),
    FOREIGN KEY (tenant_id) REFERENCES tenants(tenant_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE residence (
    tenant_id INT NOT NULL UNIQUE, 
    arrival DATE NOT NULL,
    departure DATE,
    FOREIGN KEY (tenant_id) REFERENCES tenants(tenant_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE residence_history (
    id INT AUTO_INCREMENT,
    apartment_id INT NOT NULL,
    first_name VARCHAR(30) NOT NULL,        
    last_name VARCHAR(30) NOT NULL,
    phone_number VARCHAR(20) NOT NULL, 
    total_payment INT NOT NULL,
    arrival DATE NOT NULL,
    departure DATE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (apartment_id) REFERENCES apartments(apartment_id) ON DELETE CASCADE ON UPDATE CASCADE
);