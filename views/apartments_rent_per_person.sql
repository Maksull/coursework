CREATE VIEW apartments_rent_per_person
AS
SELECT apartments.apartment_id, ROUND((apartments_rent_per_day.rent_per_day/apartments_n_tenants.n_tenants) DIV 1, -1) AS rent_per_person FROM apartments
INNER JOIN apartments_n_tenants ON apartments_n_tenants.apartment_id = apartments.apartment_id
INNER JOIN apartments_rent_per_day ON apartments_rent_per_day.apartment_id = apartments.apartment_id
ORDER BY apartment_id;


