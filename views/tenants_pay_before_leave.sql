CREATE VIEW tenants_pay_before_leave
AS
SELECT  tenants.tenant_id,
        (DATEDIFF(residence.departure, residence.arrival) * apartments_rent_per_person.rent_per_person) + paid.debt AS pay_before_leave
        FROM tenants
INNER JOIN apartments_n_tenants ON apartments_n_tenants.apartment_id = tenants.apartment_id
INNER JOIN residence ON residence.tenant_id = tenants.tenant_id
INNER JOIN apartments_rent_per_person ON apartments_rent_per_person.apartment_id = tenants.apartment_id
INNER JOIN (SELECT tenant_id, SUM(tenants_payments.rent - tenants_payments.payment) AS debt FROM tenants_payments GROUP BY tenant_id) paid
            ON paid.tenant_id = tenants.tenant_id
ORDER BY tenant_id;