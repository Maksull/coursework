CREATE VIEW houses_n_tenants
AS
SELECT houses.house_id, COUNT(apartments.apartment_id) AS n_apartments, COUNT(tenants.tenant_id) AS n_tenants FROM houses 
INNER JOIN apartments ON houses.house_id = apartments.house_id 
INNER JOIN tenants ON apartments.apartment_id = tenants.apartment_id
GROUP BY house_id;