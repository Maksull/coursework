CREATE VIEW tenants_remain_days
AS
SELECT tenants.tenant_id, DATEDIFF(residence.departure, residence.arrival) AS days_remain FROM tenants
INNER JOIN residence ON residence.tenant_id = tenants.tenant_id
ORDER BY tenant_id;