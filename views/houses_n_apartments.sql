CREATE VIEW houses_n_apartments
AS
SELECT houses.house_id, COUNT(apartments.apartment_id) AS n_apartments FROM houses INNER JOIN apartments ON houses.house_id = apartments.house_id GROUP BY house_id;