CREATE VIEW apartments_rent_per_day
AS
SELECT  apartments.apartment_id, 
        ROUND(( (apartments_total_costs.apartment_total_costs + (houses_total_costs.house_cost/houses_n_apartments.n_apartments)) * 1.25) DIV 1, -2) AS rent_per_day 
FROM apartments
INNER JOIN apartments_total_costs ON apartments_total_costs.apartment_id = apartments.apartment_id
INNER JOIN houses_total_costs ON houses_total_costs.house_id = apartments.house_id
INNER JOIN houses_n_apartments ON houses_n_apartments.house_id = apartments.house_id
GROUP BY apartment_id;


