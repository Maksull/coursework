CREATE VIEW houses_total_costs
AS
SELECT  houses.house_id, (houses_costs.house_cost), 
        SUM(apartments_total_costs.apartment_total_costs) AS apartments_costs, 
        (house_cost + SUM(apartments_total_costs.apartment_total_costs)) AS total_house_cost 
FROM houses
INNER JOIN (SELECT house_id, SUM(houses_costs.cost) AS house_cost FROM houses_costs GROUP BY house_id) houses_costs ON houses_costs.house_id = houses.house_id
INNER JOIN apartments_total_costs ON apartments_total_costs.apartment_id IN (SELECT apartment_id FROM apartments WHERE house_id = houses.house_id)
GROUP BY house_id;
