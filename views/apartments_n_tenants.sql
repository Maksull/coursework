CREATE VIEW apartments_n_tenants
AS
SELECT apartments.apartment_id, COUNT(tenants.tenant_id) AS n_tenants FROM apartments INNER JOIN tenants ON apartments.apartment_id = tenants.apartment_id GROUP BY apartment_id;
