CREATE VIEW apartments_total_costs
AS
SELECT apartment_id, SUM(cost) AS apartment_total_costs FROM apartments_costs GROUP BY apartment_id;