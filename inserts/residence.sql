INSERT INTO residence(tenant_id, arrival, departure)
VALUES (1, "2022-05-18", "2022-06-04");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (2, "2022-05-20", "2022-06-15");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (3, "2022-05-21", "2022-06-17");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (4, "2022-05-13", "2022-06-10");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (5, "2022-05-19", "2022-06-13");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (6, "2022-05-17", "2022-06-05");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (7, "2022-05-12", "2022-06-12");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (8, "2022-05-15", "2022-06-09");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (9, "2022-05-20", "2022-06-03");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (10, "2022-05-19", "2022-06-01");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (11, "2022-05-21", "2022-06-08");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (12, "2022-05-22", "2022-06-16");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (13, "2022-05-17", "2022-06-10");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (14, "2022-05-18", "2022-06-11");
INSERT INTO residence(tenant_id, arrival, departure)
VALUES (15, "2022-05-19", "2022-06-08");