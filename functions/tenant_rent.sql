CREATE FUNCTION TENANT_RENT(tenant_id INT) RETURNS int
BEGIN
    DECLARE tenant_rent INT;
    SET tenant_rent = ( SELECT apartments_rent_per_person.rent_per_person FROM tenants
                        INNER JOIN apartments_rent_per_person ON apartments_rent_per_person.apartment_id = tenants.apartment_id
                        WHERE tenants.tenant_id = tenant_id);
    return tenant_rent;
END;